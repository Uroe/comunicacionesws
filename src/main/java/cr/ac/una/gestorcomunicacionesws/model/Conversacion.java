/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorcomunicacionesws.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "MENS_CONVERSACIONES", schema = "SIGECEUNA")
@NamedQueries({
    @NamedQuery(name = "Conversacion.findAll", query = "SELECT c FROM Conversacion c"),
    @NamedQuery(name = "Conversacion.findByConId", query = "SELECT c FROM Conversacion c WHERE c.id = :id"),
    @NamedQuery(name = "Conversacion.findByConIdemisor", query = "SELECT c FROM Conversacion c WHERE c.idEmisor = :idEmisor"),
    @NamedQuery(name = "Conversacion.findByConIdreceptor", query = "SELECT c FROM Conversacion c WHERE c.idReceptor = :idReceptor")
})
public class Conversacion implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "MENS_CONVERSACIONES_CON_ID_GENERATOR", sequenceName = "SIGECEUNA.MENS_CONVERSACIONES_SEQ01", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MENS_CONVERSACIONES_CON_ID_GENERATOR")
    @Basic(optional = false)
    @Column(name = "CON_ID")
    private Long id;
    @Basic(optional = false)
    @Column(name = "CON_IDEMISOR")
    private Long idEmisor;
    @Basic(optional = false)
    @Column(name = "CON_IDRECEPTOR")
    private Long idReceptor;
    @Version
    @Column(name = "CON_VERSION")
    private Long version;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "conversacion")
    private List<MensajeEnviado> mensajesEnviados;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "conversacion")
    private List<MensajeRecibido> mensajesRecibidos;

    public Conversacion() {
    }

    public Conversacion(Long conId) {
        this.id = conId;
    }

    public Conversacion(Long conId, Long conIdemisor, Long conIdreceptor) {
        this.id = conId;
        this.idEmisor = conIdemisor;
        this.idReceptor = conIdreceptor;
    }

    public Conversacion(ConversacionDto conversacionDto) {
        this.id = conversacionDto.getConId();
        this.idEmisor = conversacionDto.getConIdEmisor();
        this.idReceptor = conversacionDto.getConIdReceptor();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdEmisor() {
        return idEmisor;
    }

    public void setIdEmisor(Long idEmisor) {
        this.idEmisor = idEmisor;
    }

    public Long getIdReceptor() {
        return idReceptor;
    }

    public void setIdReceptor(Long idReceptor) {
        this.idReceptor = idReceptor;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public List<MensajeEnviado> getMensajesEnviados() {
        return mensajesEnviados;
    }

    public void setMensajesEnviados(List<MensajeEnviado> mensajesEnviados) {
        this.mensajesEnviados = mensajesEnviados;
    }

    public List<MensajeRecibido> getMensajesRecibidos() {
        return mensajesRecibidos;
    }

    public void setMensajesRecibidos(List<MensajeRecibido> mensajesRecibidos) {
        this.mensajesRecibidos = mensajesRecibidos;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Conversacion)) {
            return false;
        }
        Conversacion other = (Conversacion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cr.ac.una.gestorcomunicacionesws.model.Conversacion[ conId=" + id + " ]";
    }

}
