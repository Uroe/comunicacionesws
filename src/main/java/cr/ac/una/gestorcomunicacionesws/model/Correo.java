/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorcomunicacionesws.model;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "GESCOR_CORREOS", schema = "SIGECEUNA")
@NamedQueries({
    @NamedQuery(name = "Correo.findAll", query = "SELECT c FROM Correo c"),
    @NamedQuery(name = "Correo.findByCorId", query = "SELECT c FROM Correo c WHERE c.id = :id"),
    @NamedQuery(name = "Correo.findByCorIdusuario", query = "SELECT c FROM Correo c WHERE c.idUsuario = :idUsuario")})
public class Correo implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "GESCOR_CORREOS_COR_ID_GENERATOR", sequenceName = "SIGECEUNA.GESCOR_CORREOS_SEQ01", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GESCOR_CORREOS_COR_ID_GENERATOR")
    @Basic(optional = false)
    @Column(name = "COR_ID")
    private Long id;
    @Basic(optional = false)
    @Column(name = "COR_DESTINATARIO")
    private String destinatario;
    @Basic(optional = false)
    @Column(name = "COR_ASUNTO")
    private String asunto;
    @Lob
    @Basic(optional = false)
    @Column(name = "COR_CONTENIDO")
    private String contenido;
    @Basic(optional = false)
    @Column(name = "COR_IDUSUARIO")
    private Long idUsuario;
    @Basic(optional = false)
    @Column(name = "COR_FECHA")
    private LocalDate fecha;
    @Basic(optional = false)
    @Column(name = "COR_ESTADO")
    private String estado;
    @Version
    @Column(name = "COR_VERSION")
    private Long version;

    public Correo() {
    }

    public Correo(Long corId) {
        this.id = corId;
    }

    public Correo(Long corId, String corDestinatario, String corAsunto, String corContenido, Long corIdusuario, LocalDate corFecha, String corEstado) {
        this.id = corId;
        this.destinatario = corDestinatario;
        this.asunto = corAsunto;
        this.contenido = corContenido;
        this.idUsuario = corIdusuario;
        this.fecha = corFecha;
        this.estado = corEstado;
    }

    public void actualizarCorreo(CorreoDto cd) {
        this.destinatario = cd.getCorDestinatario();
        this.asunto = cd.getCorAsunto();
        this.contenido = cd.getCorContenido();
        this.fecha = cd.getCorFecha();
        this.estado = cd.getCorEstado();
    }

    public Correo(CorreoDto cd) {
        this.id = cd.getCorId();
        actualizarCorreo(cd);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDestinatario() {
        return destinatario;
    }

    public void setDestinatario(String destinatario) {
        this.destinatario = destinatario;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Correo)) {
            return false;
        }
        Correo other = (Correo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cr.ac.una.gestorcorreosws.model.Correo[ corId=" + id + " ]";
    }

}
