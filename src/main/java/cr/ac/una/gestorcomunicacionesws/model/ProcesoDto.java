/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorcomunicacionesws.model;

/**
 *
 * @author Usuario
 */
public class ProcesoDto {

    private Long proId;
    private String proNombre;
    private String proMachote;
    private Boolean modificado;

    public ProcesoDto() {
        this.modificado = false;
    }

    public ProcesoDto(Proceso proceso) {
        this();
        this.proId = proceso.getId();
        this.proNombre = proceso.getNombre();
        this.proMachote = proceso.getMachote();
    }

    public Long getProId() {
        return proId;
    }

    public void setProId(Long proId) {
        this.proId = proId;
    }

    public String getProNombre() {
        return proNombre;
    }

    public void setProNombre(String proNombre) {
        this.proNombre = proNombre;
    }

    public String getProMachote() {
        return proMachote;
    }

    public void setProMachote(String proMachote) {
        this.proMachote = proMachote;
    }

    public Boolean getModificado() {
        return modificado;
    }

    public void setModificado(Boolean modificado) {
        this.modificado = modificado;
    }

}
