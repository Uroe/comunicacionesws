/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorcomunicacionesws.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "MENS_MENSAJESENVIADOS", schema = "SIGECEUNA")
@NamedQueries({
    @NamedQuery(name = "MensajeEnviado.findAll", query = "SELECT m FROM MensajeEnviado m"),
    @NamedQuery(name = "MensajeEnviado.findByMenenvId", query = "SELECT m FROM MensajeEnviado m WHERE m.id = :id"),
    @NamedQuery(name = "MensajeEnviado.findByMenenvIdemisor", query = "SELECT m FROM MensajeEnviado m WHERE m.idEmisor = :idEmisor")
})
public class MensajeEnviado implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "MENS_MENSAJESENVIADOS_MENENV_ID_GENERATOR", sequenceName = "SIGECEUNA.MENS_MENSAJESENVIADOS_SEQ01", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MENS_MENSAJESENVIADOS_MENENV_ID_GENERATOR")
    @Basic(optional = false)
    @Column(name = "MENENV_ID")
    private Long id;
    @Basic(optional = false)
    @Column(name = "MENENV_IDEMISOR")
    private Long idEmisor;
    @Basic(optional = false)
    @Column(name = "MENENV_MENSAJE")
    private String mensaje;
    @Version
    @Column(name = "MENENV_VERSION")
    private Long version;
    @JoinColumn(name = "MENENV_IDCON", referencedColumnName = "CON_ID")
    @ManyToOne(optional = false)
    private Conversacion conversacion;

    public MensajeEnviado() {
    }

    public MensajeEnviado(Long menenvId) {
        this.id = menenvId;
    }

    public MensajeEnviado(Long menenvId, Long menenvIdemisor, String menenvMensaje) {
        this.id = menenvId;
        this.idEmisor = menenvIdemisor;
        this.mensaje = menenvMensaje;
    }

    public MensajeEnviado(MensajeEnviadoDto mensajeEnviadoDto) {
        this.id = mensajeEnviadoDto.getId();
        this.idEmisor = mensajeEnviadoDto.getIdEmisor();
        this.mensaje = mensajeEnviadoDto.getMensaje();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdEmisor() {
        return idEmisor;
    }

    public void setIdEmisor(Long idEmisor) {
        this.idEmisor = idEmisor;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Conversacion getConversacion() {
        return conversacion;
    }

    public void setConversacion(Conversacion conversacion) {
        this.conversacion = conversacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MensajeEnviado)) {
            return false;
        }
        MensajeEnviado other = (MensajeEnviado) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cr.ac.una.gestorcomunicacionesws.model.MensajeEnviado[ menenvId=" + id + " ]";
    }

}
