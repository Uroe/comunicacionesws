/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorcomunicacionesws.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "GESCOR_PROCESOS", schema = "SIGECEUNA")
@NamedQueries({
    @NamedQuery(name = "Proceso.findAll", query = "SELECT p FROM Proceso p"),
    @NamedQuery(name = "Proceso.findByProId", query = "SELECT p FROM Proceso p WHERE p.id = :id")
})
public class Proceso implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "GESCOR_PROCESOS_PRO_ID_GENERATOR", sequenceName = "SIGECEUNA.GESCOR_PROCESOS_SEQ01", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GESCOR_PROCESOS_PRO_ID_GENERATOR")
    @Basic(optional = false)
    @Column(name = "PRO_ID")
    private Long id;
    @Basic(optional = false)
    @Column(name = "PRO_NOMBRE")
    private String nombre;
    @Lob
    @Basic(optional = false)
    @Column(name = "PRO_MACHOTE")
    private String machote;
    @Version
    @Column(name = "PRO_VERSION")
    private Long version;

    public Proceso() {
    }

    public Proceso(Long proId) {
        this.id = proId;
    }

    public Proceso(Long proId, String proNombre, String proMachote) {
        this.id = proId;
        this.nombre = proNombre;
        this.machote = proMachote;
    }

    public Proceso(ProcesoDto procesoDto) {
        this.id = procesoDto.getProId();
        actualizarProceso(procesoDto);
    }

    public void actualizarProceso(ProcesoDto procesoDto) {
        this.nombre = procesoDto.getProNombre();
        this.machote = procesoDto.getProMachote();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMachote() {
        return machote;
    }

    public void setMachote(String machote) {
        this.machote = machote;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Proceso)) {
            return false;
        }
        Proceso other = (Proceso) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cr.ac.una.gestorcorreosws.model.Proceso[ proId=" + id + " ]";
    }

}
