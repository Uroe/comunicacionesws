/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorcomunicacionesws.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 *
 * @author Usuario
 */
public class CorreoDto {

    private Long corId;
    private String corDestinatario;
    private String corAsunto;
    private String corContenido;
    private Long corIdUsuario;
    private LocalDate corFecha;
    private String corEstado;
    private Boolean modificado;
//    private LocalDateTime fecha;

    public CorreoDto() {
        this.modificado = false;
//        this.fecha = LocalDateTime.now();
    }

    public CorreoDto(Correo correo) {
        this();
        this.corId = correo.getId();
        this.corDestinatario = correo.getDestinatario();
        this.corAsunto = correo.getAsunto();
        this.corContenido = correo.getContenido();
        this.corFecha = correo.getFecha();
        this.corEstado = correo.getEstado();
//        this.fecha = LocalDateTime.now();
    }

    public Long getCorId() {
        return corId;
    }

    public void setCorId(Long corId) {
        this.corId = corId;
    }

    public String getCorDestinatario() {
        return corDestinatario;
    }

    public void setCorDestinatario(String corDestinatario) {
        this.corDestinatario = corDestinatario;
    }

    public String getCorAsunto() {
        return corAsunto;
    }

    public void setCorAsunto(String corAsunto) {
        this.corAsunto = corAsunto;
    }

    public String getCorContenido() {
        return corContenido;
    }

    public void setCorContenido(String corContenido) {
        this.corContenido = corContenido;
    }

    public Long getCorIdUsuario() {
        return corIdUsuario;
    }

    public void setCorIdUsuario(Long corIdUsuario) {
        this.corIdUsuario = corIdUsuario;
    }

    public LocalDate getCorFecha() {
        return corFecha;
    }

    public void setCorFecha(LocalDate corFecha) {
        this.corFecha = corFecha;
    }

    public String getCorEstado() {
        return corEstado;
    }

    public void setCorEstado(String corEstado) {
        this.corEstado = corEstado;
    }

    public Boolean getModificado() {
        return modificado;
    }

    public void setModificado(Boolean modificado) {
        this.modificado = modificado;
    }

//    public LocalDateTime getFecha() {
//        return fecha;
//    }
//
//    public void setFecha(LocalDateTime fecha) {
//        this.fecha = fecha;
//    }
    
}
