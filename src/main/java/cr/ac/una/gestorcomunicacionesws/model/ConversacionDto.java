/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorcomunicacionesws.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ale
 */
public class ConversacionDto {
    
    private Long conId;
    private Long conIdEmisor;
    private Long conIdReceptor;
    List<MensajeEnviadoDto> mensajesEnviados;
    List<MensajeRecibidoDto> mensajesRecibidos;
    private Boolean modificado;
    
    public ConversacionDto() {
        this.modificado = false;
        mensajesEnviados = new ArrayList<>();
        mensajesRecibidos = new ArrayList<>();
    }
    
    public ConversacionDto(Conversacion conversacion) {
        this();
        this.conId = conversacion.getId();
        this.conIdEmisor = conversacion.getIdEmisor();
        this.conIdReceptor = conversacion.getIdReceptor();
    }

    public Long getConId() {
        return conId;
    }

    public void setConId(Long conId) {
        this.conId = conId;
    }

    public Long getConIdEmisor() {
        return conIdEmisor;
    }

    public void setConIdEmisor(Long conIdEmisor) {
        this.conIdEmisor = conIdEmisor;
    }

    public Long getConIdReceptor() {
        return conIdReceptor;
    }

    public void setConIdReceptor(Long conIdReceptor) {
        this.conIdReceptor = conIdReceptor;
    }

    public List<MensajeEnviadoDto> getMensajesEnviados() {
        return mensajesEnviados;
    }

    public void setMensajesEnviados(List<MensajeEnviadoDto> mensajesEnviados) {
        this.mensajesEnviados = mensajesEnviados;
    }

    public List<MensajeRecibidoDto> getMensajesRecibidos() {
        return mensajesRecibidos;
    }

    public void setMensajesRecibidos(List<MensajeRecibidoDto> mensajesRecibidos) {
        this.mensajesRecibidos = mensajesRecibidos;
    }

    public Boolean getModificado() {
        return modificado;
    }

    public void setModificado(Boolean modificado) {
        this.modificado = modificado;
    }

    @Override
    public String toString() {
        return "ConversacionDto{" + "conId=" + conId + ", conIdEmisor=" + conIdEmisor + ", conIdReceptor=" + conIdReceptor + ", mensajesEnviados=" + mensajesEnviados + ", mensajesRecibidos=" + mensajesRecibidos + ", modificado=" + modificado + '}';
    }
    
}
