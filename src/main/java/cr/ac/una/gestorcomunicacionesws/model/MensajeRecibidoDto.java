/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorcomunicacionesws.model;

/**
 *
 * @author JosueNG
 */
public class MensajeRecibidoDto {

    private Long id;
    private Long idReceptor;
    private String mensaje;
    private Boolean modificado;
    private ConversacionDto conversacion;

    public MensajeRecibidoDto() {
        this.modificado = false;
        conversacion = new ConversacionDto();
    }

    public MensajeRecibidoDto(MensajeRecibido mensajeRecibido) {
        this();
        this.id = mensajeRecibido.getId();
        this.idReceptor = mensajeRecibido.getIdReceptor();
        this.mensaje = mensajeRecibido.getMensaje();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdReceptor() {
        return idReceptor;
    }

    public void setIdReceptor(Long idReceptor) {
        this.idReceptor = idReceptor;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Boolean getModificado() {
        return modificado;
    }

    public void setModificado(Boolean modificado) {
        this.modificado = modificado;
    }

    public ConversacionDto getConversacion() {
        return conversacion;
    }

    public void setConversacion(ConversacionDto conversacion) {
        this.conversacion = conversacion;
    }

    @Override
    public String toString() {
        return "MensajesRecibidosDto{" + "id=" + id + ", idReceptor=" + idReceptor + ", mensaje=" + mensaje + '}';
    }

}
