/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorcomunicacionesws.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "MENS_MENSAJESRECIBIDOS", schema = "SIGECEUNA")
@NamedQueries({
    @NamedQuery(name = "MensajeRecibido.findAll", query = "SELECT m FROM MensajeRecibido m"),
    @NamedQuery(name = "MensajeRecibido.findByMenrecId", query = "SELECT m FROM MensajeRecibido m WHERE m.id = :id"),
    @NamedQuery(name = "MensajeRecibido.findByMenrecIdreceptor", query = "SELECT m FROM MensajeRecibido m WHERE m.idReceptor = :idReceptor")
})
public class MensajeRecibido implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "MENS_MENSAJESRECIBIDOS_MENREC_ID_GENERATOR", sequenceName = "SIGECEUNA.MENS_MENSAJESRECIBIDOS_SEQ01", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MENS_MENSAJESRECIBIDOS_MENREC_ID_GENERATOR")
    @Basic(optional = false)
    @Column(name = "MENREC_ID")
    private Long id;
    @Basic(optional = false)
    @Column(name = "MENREC_IDRECEPTOR")
    private Long idReceptor;
    @Basic(optional = false)
    @Column(name = "MENREC_MENSAJE")
    private String mensaje;
    @Version
    @Column(name = "MENREC_VERSION")
    private Long version;
    @JoinColumn(name = "MENREC_IDCON", referencedColumnName = "CON_ID")
    @ManyToOne(optional = false)
    private Conversacion conversacion;

    public MensajeRecibido() {
    }

    public MensajeRecibido(Long menrecId) {
        this.id = menrecId;
    }

    public MensajeRecibido(Long menrecId, Long menrecIdreceptor, String menrecMensaje) {
        this.id = menrecId;
        this.idReceptor = menrecIdreceptor;
        this.mensaje = menrecMensaje;
    }
    
    public MensajeRecibido(MensajeRecibidoDto mensajeRecibidoDto){
        this.id = mensajeRecibidoDto.getId();
        this.idReceptor = mensajeRecibidoDto.getIdReceptor();
        this.mensaje = mensajeRecibidoDto.getMensaje();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdReceptor() {
        return idReceptor;
    }

    public void setIdReceptor(Long idReceptor) {
        this.idReceptor = idReceptor;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Conversacion getConversacion() {
        return conversacion;
    }

    public void setConversacion(Conversacion conversacion) {
        this.conversacion = conversacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MensajeRecibido)) {
            return false;
        }
        MensajeRecibido other = (MensajeRecibido) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cr.ac.una.gestorcomunicacionesws.model.MensajeRecibido[ menrecId=" + id + " ]";
    }

}
