/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorcomunicacionesws.model;

/**
 *
 * @author JosueNG
 */
public class MensajeEnviadoDto {

    private Long id;
    private Long idEmisor;
    private String mensaje;
    private Boolean modificado;
        private ConversacionDto conversacion;

    public MensajeEnviadoDto() {
        this.modificado = false;
                conversacion = new ConversacionDto();
    }

    public MensajeEnviadoDto(MensajeEnviado mensajeEnviado) {
        this();
        this.id = mensajeEnviado.getId();
        this.idEmisor = mensajeEnviado.getIdEmisor();
        this.mensaje = mensajeEnviado.getMensaje();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdEmisor() {
        return idEmisor;
    }

    public void setIdEmisor(Long idEmisor) {
        this.idEmisor = idEmisor;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Boolean getModificado() {
        return modificado;
    }

    public void setModificado(Boolean modificado) {
        this.modificado = modificado;
    }

    public ConversacionDto getConversacion() {
        return conversacion;
    }

    public void setConversacion(ConversacionDto conversacion) {
        this.conversacion = conversacion;
    }
    @Override
    public String toString() {
        return "MensajesEnvidosDto{" + "id=" + id + ", idEmisor=" + idEmisor + ", mensaje=" + mensaje + '}';
    }

}
