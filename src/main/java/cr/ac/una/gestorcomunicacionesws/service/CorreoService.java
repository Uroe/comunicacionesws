/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorcomunicacionesws.service;

import cr.ac.una.gestorcomunicacionesws.model.Correo;
import cr.ac.una.gestorcomunicacionesws.model.CorreoDto;
import cr.ac.una.gestorcomunicacionesws.util.CodigoRespuesta;
import cr.ac.una.gestorcomunicacionesws.util.Respuesta;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Usuario
 */
@Stateless
@LocalBean
public class CorreoService {

    private static final Logger LOG = Logger.getLogger(CorreoService.class.getName());

    @PersistenceContext(unitName = "GestorComunicacionesWsPU")
    private EntityManager em;

    public Respuesta getCorreo(Long id) {
        try {
            Query qryCorreo = em.createNamedQuery("Correo.findByCorId", Correo.class);
            qryCorreo.setParameter("id", id);

            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Correo", new CorreoDto((Correo) qryCorreo.getSingleResult()));

        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existe un correo con el código ingresado.", "getCorreo NoResultException");
        } catch (NonUniqueResultException ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el correo.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el correo.", "getCorreo NonUniqueResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el correo.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el correo.", "getCorreo " + ex.getMessage());
        }
    }

    public Respuesta getCorreos() {
        try {
            Query qryCorreo = em.createNamedQuery("Correo.findAll", Correo.class);
            List<Correo> correos = qryCorreo.getResultList();

            List<CorreoDto> correoDtos = new ArrayList<>();
            for (Correo correo : correos) {
                correoDtos.add(new CorreoDto(correo));
            }
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Correos", correoDtos);

        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existen correos con los criterios ingresados.", "getCorreos NoResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el correo.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el correo.", "getCorreos " + ex.getMessage());
        }
    }

    public Respuesta guardarCorreo(CorreoDto correoDto) {
        try {
            Correo correo;
            if (correoDto.getCorId() != null && correoDto.getCorId() > 0) {
                correo = em.find(Correo.class, correoDto.getCorId());
                if (correo == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontró el correo a modificar.", "guardarCorreo NoResultException");
                }
                correo.actualizarCorreo(correoDto);
                correo = em.merge(correo);
            } else {
                correo = new Correo(correoDto);
                em.persist(correo);
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Correo", new CorreoDto(correo));
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar el correo.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al guardar el correo.", "guardarCorreo " + ex.getMessage());
        }
    }
    
    public Respuesta eliminarCorreo(Long id) {
        try {
            Correo correo;
            if (id != null && id > 0) {
                correo = em.find(Correo.class, id);
                if (correo == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontró el correo a eliminar.", "eliminarCorreo NoResultException");
                }
                em.remove(correo);
            } else {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Debe cargar el correo a eliminar.", "eliminarProceso NoResultException");
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "");
        } catch (Exception ex) {
            if (ex.getCause() != null && ex.getCause().getCause().getClass() == SQLIntegrityConstraintViolationException.class) {
                return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "No se puede eliminar el correo porque tiene relaciones con otros registros.", "eliminarProceso " + ex.getMessage());
            }
            LOG.log(Level.SEVERE, "Ocurrio un error al eliminar el correo.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al eliminar el correo.", "eliminarCorreo " + ex.getMessage());
        }
    }
}
