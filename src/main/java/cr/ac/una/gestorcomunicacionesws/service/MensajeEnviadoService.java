/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorcomunicacionesws.service;

import cr.ac.una.gestorcomunicacionesws.model.Conversacion;
import cr.ac.una.gestorcomunicacionesws.model.MensajeEnviado;
import cr.ac.una.gestorcomunicacionesws.model.MensajeEnviadoDto;
import cr.ac.una.gestorcomunicacionesws.util.CodigoRespuesta;
import cr.ac.una.gestorcomunicacionesws.util.Respuesta;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Usuario
 */
@Stateless
@LocalBean
public class MensajeEnviadoService {

    private static final Logger LOG = Logger.getLogger(MensajeEnviadoService.class.getName());

    @PersistenceContext(unitName = "GestorComunicacionesWsPU")
    private EntityManager em;

    public Respuesta getMensajeEnviado(Long id) {
        try {
            Query qryMensajeEnviado = em.createNamedQuery("MensajeEnviado.findByMenenvId", MensajeEnviado.class);
            qryMensajeEnviado.setParameter("id", id);

            MensajeEnviado me = (MensajeEnviado) qryMensajeEnviado.getSingleResult();
            MensajeEnviadoDto med = new MensajeEnviadoDto(me);
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "MensajeEnviado", med);

        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existe un mensaje con el codigo ingresado.", "getMensajeEnviado NoResultException");
        } catch (NonUniqueResultException ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el mensaje.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error.", "getMensajeEnviado NonUniqueResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el mensaje.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el mensaje.", "getMensajeEnviado " + ex.getMessage());
        }
    }

    public Respuesta getMensajesEnviados(Long id) {
        try {
            Query qryMed = em.createNamedQuery("MensajeEnviado.findAll", MensajeEnviado.class);

            List<MensajeEnviado> mensajes = qryMed.getResultList();
            List<MensajeEnviado> mensajesxConv = new ArrayList<>();
            Conversacion conversacion;

            for (MensajeEnviado me : mensajes) {
                conversacion = me.getConversacion();
                if (Objects.equals(conversacion.getId(), id)) {
                    mensajesxConv.add(me);
                }
            }

            List<MensajeEnviadoDto> meds = new ArrayList<>();
            for (MensajeEnviado me : mensajesxConv) {
                meds.add(new MensajeEnviadoDto(me));
            }
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "MensajesEnviados", meds);

        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existen mensajes con los criterios ingresados.", "getMensajesEnviados NoResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el mensaje.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el mensaje.", "getMensajesEnviados " + ex.getMessage());
        }
    }

    public Respuesta guardarMensajeEnviado(MensajeEnviadoDto med) {
        try {
            Conversacion conversacion = em.find(Conversacion.class, med.getConversacion().getConId());
            if (conversacion == null) {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontró el mensaje a modificar", "guardarMensajeEnviado NoResultException");
            } else {
                MensajeEnviado me;
                if (med.getId() != null && med.getId() > 0) {
                    me = em.find(MensajeEnviado.class, med.getId());
                    if (me == null) {
                        return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontró el mensaje a modificar.", "guardarMensajeEnviado NoResultException");
                    }
                    me.setConversacion(conversacion);
                    me = em.merge(me);
                } else {
                    me = new MensajeEnviado(med);
                    me.setConversacion(conversacion);
                    em.persist(me);
                }
                em.flush();
                return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "MensajeEnviado", new MensajeEnviadoDto(me));
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar el mensaje.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al guardar el mensaje.", "guardarMensajeEnviado " + ex.getMessage());
        }
    }

    public Respuesta eliminarMensajeEnviado(Long id) {
        try {
            MensajeEnviado me;
            if (id != null && id > 0) {
                me = em.find(MensajeEnviado.class, id);
                if (me == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontró el mensaje a eliminar.", "eliminarMensajeEnviado NoResultException");
                }
                em.remove(me);
            } else {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Debe cargar el mensaje a eliminar.", "eliminarMensajeEnviado NoResultException");
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "");
        } catch (Exception ex) {
            if (ex.getCause() != null && ex.getCause().getCause().getClass() == SQLIntegrityConstraintViolationException.class) {
                return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "No se puede eliminar el mensaje.", "eliminarMensajeEnviado " + ex.getMessage());
            }
            LOG.log(Level.SEVERE, "Ocurrio un error al eliminar el mensaje.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al eliminar el mensaje.", "eliminarMensajeEnviado " + ex.getMessage());
        }
    }
}
