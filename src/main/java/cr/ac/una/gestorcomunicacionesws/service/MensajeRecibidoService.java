/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorcomunicacionesws.service;

import cr.ac.una.gestorcomunicacionesws.model.Conversacion;
import cr.ac.una.gestorcomunicacionesws.model.MensajeRecibido;
import cr.ac.una.gestorcomunicacionesws.model.MensajeRecibidoDto;
import cr.ac.una.gestorcomunicacionesws.util.CodigoRespuesta;
import cr.ac.una.gestorcomunicacionesws.util.Respuesta;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Usuario
 */
@Stateless
@LocalBean
public class MensajeRecibidoService {

    private static final Logger LOG = Logger.getLogger(MensajeRecibidoService.class.getName());

    @PersistenceContext(unitName = "GestorComunicacionesWsPU")
    private EntityManager em;

    public Respuesta getMensajeRecibido(Long id) {
        try {
            Query qryMensajeRecibido = em.createNamedQuery("MensajeRecibido.findByMenrecId", MensajeRecibido.class);
            qryMensajeRecibido.setParameter("id", id);

            MensajeRecibido mr = (MensajeRecibido) qryMensajeRecibido.getSingleResult();
            MensajeRecibidoDto mrd = new MensajeRecibidoDto(mr);
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "MensajeRecibido", mrd);

        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existe un mensaje con el código ingresado.", "getMensajeRecibido NoResultException");
        } catch (NonUniqueResultException ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el mensaje.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el mensaje.", "getMensajeRecibido NonUniqueResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el mensaje.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el mensaje.", "getMensajeRecibido " + ex.getMessage());
        }
    }

    public Respuesta getMensajesRecibidos(Long id) {
        try {
            Query qryMrd = em.createNamedQuery("MensajeRecibido.findAll", MensajeRecibido.class);

            List<MensajeRecibido> mensajes = qryMrd.getResultList();
            List<MensajeRecibido> mensajesxConv = new ArrayList<>();
            Conversacion conversacion;

            for (MensajeRecibido mr : mensajes) {
                conversacion = mr.getConversacion();
                if (Objects.equals(conversacion.getId(), id)) {
                    mensajesxConv.add(mr);
                }
            }

            List<MensajeRecibidoDto> merd = new ArrayList<>();
            for (MensajeRecibido mr : mensajesxConv) {
                merd.add(new MensajeRecibidoDto(mr));
            }
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "MensajesRecibidos", merd);

        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existen mensajes con los criterios ingresados.", "getMensajesRecibidos NoResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar los mensajes.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar los mensajes.", "getMensajesRecibidos " + ex.getMessage());
        }
    }

    public Respuesta guardarMensajeRecibido(MensajeRecibidoDto mrd) {
        try {
            Conversacion conversacion = em.find(Conversacion.class, mrd.getConversacion().getConId());
            if (conversacion == null) {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontró el mensaje a modificar ", "guardarMensajeRecibido NoResultException");
            } else {
                MensajeRecibido mr;
                if (mrd.getId() != null && mrd.getId() > 0) {
                    mr = em.find(MensajeRecibido.class, mrd.getId());
                    if (mr == null) {
                        return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontró el mensaje a modificar.", "guardarMensajeRecibido NoResultException");
                    }
                    mr.setConversacion(conversacion);
                    mr = em.merge(mr);
                } else {
                    mr = new MensajeRecibido(mrd);
                    mr.setConversacion(conversacion);
                    em.persist(mr);
                }
                em.flush();
                return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "MensajeRecibido", new MensajeRecibidoDto(mr));
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar el mensaje.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al guardar el mensaje.", "guardarMensajeRecibido " + ex.getMessage());
        }
    }

    public Respuesta eliminarMensajeRecibido(Long id) {
        try {
            MensajeRecibido mr;
            if (id != null && id > 0) {
                mr = em.find(MensajeRecibido.class, id);
                if (mr == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontró el mensaje a eliminar.", "eliminarMensajeRecibido NoResultException");
                }
                em.remove(mr);
            } else {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Debe cargar el mensaje a eliminar.", "eliminarMensajeRecibido NoResultException");
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "");
        } catch (Exception ex) {
            if (ex.getCause() != null && ex.getCause().getCause().getClass() == SQLIntegrityConstraintViolationException.class) {
                return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "No se puede eliminar el mensaje.", "eliminarMensajeRecibido " + ex.getMessage());
            }
            LOG.log(Level.SEVERE, "Ocurrio un error al eliminar el mensaje.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al eliminar el mensaje.", "eliminarMensajeRecibido " + ex.getMessage());
        }
    }
}
