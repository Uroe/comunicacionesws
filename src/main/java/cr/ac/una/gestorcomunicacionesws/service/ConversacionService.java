/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorcomunicacionesws.service;

import cr.ac.una.gestorcomunicacionesws.model.Conversacion;
import cr.ac.una.gestorcomunicacionesws.model.ConversacionDto;
import cr.ac.una.gestorcomunicacionesws.model.MensajeEnviado;
import cr.ac.una.gestorcomunicacionesws.model.MensajeEnviadoDto;
import cr.ac.una.gestorcomunicacionesws.model.MensajeRecibido;
import cr.ac.una.gestorcomunicacionesws.model.MensajeRecibidoDto;
import cr.ac.una.gestorcomunicacionesws.util.CodigoRespuesta;
import cr.ac.una.gestorcomunicacionesws.util.Respuesta;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Usuario
 */
@Stateless
@LocalBean
public class ConversacionService {

    private static final Logger LOG = Logger.getLogger(ConversacionService.class.getName());

    @PersistenceContext(unitName = "GestorComunicacionesWsPU")
    private EntityManager em;

    public Respuesta getConversacion(Long id) {
        try {
            Query qryConversacion = em.createNamedQuery("Conversacion.findByConId", Conversacion.class);
            qryConversacion.setParameter("id", id);

            Conversacion conversacion = (Conversacion) qryConversacion.getSingleResult();
            ConversacionDto conversacionDto = new ConversacionDto(conversacion);
            
            for (MensajeEnviado msjEnviado : conversacion.getMensajesEnviados()) {
                conversacionDto.getMensajesEnviados().add(new MensajeEnviadoDto(msjEnviado));
            }
            for (MensajeRecibido msjRecibido : conversacion.getMensajesRecibidos()) {
                conversacionDto.getMensajesRecibidos().add(new MensajeRecibidoDto(msjRecibido));
            }

            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Conversacion", conversacionDto);

        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existe una conversacion con el código ingresado.", "getConversacion NoResultException");
        } catch (NonUniqueResultException ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar la conversacion.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar la conversacion.", "getConversacion NonUniqueResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar la conversacion.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar la conversacion.", "getConversacion " + ex.getMessage());
        }
    }

    public Respuesta getConversaciones(Long idUsuario) {
        try {
            Query qryConversaciones = em.createNamedQuery("Conversacion.findAll", Conversacion.class);
            List<Conversacion> conversaciones = qryConversaciones.getResultList();
            List<Conversacion> filtro = conversaciones.stream().filter(x -> x.getIdEmisor().equals(idUsuario) || x.getIdReceptor().equals(idUsuario)).collect(
                    Collectors.toList());
            List<ConversacionDto> conversacionesDto = new ArrayList<>();
            for (Conversacion conversacion : filtro) {
                conversacionesDto.add(new ConversacionDto(conversacion));
            }
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Conversaciones", conversacionesDto);

        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existen conversaciones con los criterios ingresados.", "getConversaciones NoResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar las conversaciones.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar las conversaciones.", "getConversaciones " + ex.getMessage());
        }
    }

    public Respuesta guardarConversacion(ConversacionDto conversacionDto) {
        try {
            Conversacion conversacion;
            if (conversacionDto.getConId() != null && conversacionDto.getConId() > 0) {
                conversacion = em.find(Conversacion.class, conversacionDto.getConId());
                if (conversacion == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontró la conversacion a modificar.", "guardarConversacion NoResultException");
                }
                
                if (!conversacionDto.getMensajesEnviados().isEmpty()) {
                    for (MensajeEnviadoDto med : conversacionDto.getMensajesEnviados()) {
                        if (med.getModificado()) {
                            MensajeEnviado me = em.find(MensajeEnviado.class, med.getId());
                            me.setConversacion(conversacion);
                            conversacion.getMensajesEnviados().add(me);
                        }
                    }
                }
                if (!conversacionDto.getMensajesRecibidos().isEmpty()) {
                    for (MensajeRecibidoDto mrd : conversacionDto.getMensajesRecibidos()) {
                        if (mrd.getModificado()) {
                            MensajeRecibido mr = em.find(MensajeRecibido.class, mrd.getId());
                            mr.setConversacion(conversacion);
                            conversacion.getMensajesRecibidos().add(mr);
                        }
                    }
                }
                conversacion = em.merge(conversacion);
            } else {
                conversacion = new Conversacion(conversacionDto);
                em.persist(conversacion);
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Conversacion", new ConversacionDto(conversacion));
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar la conversacion.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al guardar la conversacion.", "guardarConversacion " + ex.getMessage());
        }
    }

    public Respuesta eliminarConversacion(Long id) {
        try {
            Conversacion conversacion;
            if (id != null && id > 0) {
                conversacion = em.find(Conversacion.class, id);
                if (conversacion == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontró la conversacion a eliminar.", "eliminarConversacion NoResultException");
                }
                em.remove(conversacion);
            } else {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Debe cargar la conversacion a eliminar.", "eliminarConversacion NoResultException");
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "");
        } catch (Exception ex) {
            if (ex.getCause() != null && ex.getCause().getCause().getClass() == SQLIntegrityConstraintViolationException.class) {
                return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "No se puede eliminar la conversacion porque tiene relaciones con otros registros.", "eliminarConversacion " + ex.getMessage());
            }
            LOG.log(Level.SEVERE, "Ocurrio un error al eliminar la conversacion.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al eliminar la conversacion.", "eliminarConversacion " + ex.getMessage());
        }
    }
}
