/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorcomunicacionesws.service;

import cr.ac.una.gestorcomunicacionesws.model.Proceso;
import cr.ac.una.gestorcomunicacionesws.model.ProcesoDto;
import cr.ac.una.gestorcomunicacionesws.util.CodigoRespuesta;
import cr.ac.una.gestorcomunicacionesws.util.Respuesta;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Usuario
 */
@Stateless
@LocalBean
public class ProcesoService {

    private static final Logger LOG = Logger.getLogger(ProcesoService.class.getName());

    @PersistenceContext(unitName = "GestorComunicacionesWsPU")
    private EntityManager em;

    public Respuesta getProceso(Long id) {
        try {
            Query qryProceso = em.createNamedQuery("Proceso.findByProId", Proceso.class);
            qryProceso.setParameter("id", id);

            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Proceso", new ProcesoDto((Proceso) qryProceso.getSingleResult()));

        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existe un proceso con el código ingresado.", "getProceso NoResultException");
        } catch (NonUniqueResultException ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el proceso.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el proceso.", "getProceso NonUniqueResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el proceso.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el proceso.", "getProceso " + ex.getMessage());
        }
    }

    public Respuesta getProcesos() {
        try {
            Query qryProceso = em.createNamedQuery("Proceso.findAll", Proceso.class);
            List<Proceso> procesos = qryProceso.getResultList();

            List<ProcesoDto> procesoDtos = new ArrayList<>();
            for (Proceso proceso : procesos) {
                procesoDtos.add(new ProcesoDto(proceso));
            }
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Procesos", procesoDtos);

        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existen procesos con los criterios ingresados.", "getProcesos NoResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el proceso.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el proceso.", "getProcesos " + ex.getMessage());
        }
    }

    public Respuesta guardarProceso(ProcesoDto procesoDto) {
        try {
            Proceso proceso;
            if (procesoDto.getProId() != null && procesoDto.getProId() > 0) {
                proceso = em.find(Proceso.class, procesoDto.getProId());
                if (proceso == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encrontró el proceso a modificar.", "guardarProceso NoResultException");
                }
                proceso.actualizarProceso(procesoDto);
                proceso = em.merge(proceso);
            } else {
                proceso = new Proceso(procesoDto);
                em.persist(proceso);
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Proceso", new ProcesoDto(proceso));
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar el proceso.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al guardar el proceso.", "guardarProceso " + ex.getMessage());
        }
    }

    public Respuesta eliminarProceso(Long id) {
        try {
            Proceso proceso;
            if (id != null && id > 0) {
                proceso = em.find(Proceso.class, id);
                if (proceso == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encrontró el proceso a eliminar.", "eliminarProceso NoResultException");
                }
                em.remove(proceso);
            } else {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Debe cargar el proceso a eliminar.", "eliminarProceso NoResultException");
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "");
        } catch (Exception ex) {
            if (ex.getCause() != null && ex.getCause().getCause().getClass() == SQLIntegrityConstraintViolationException.class) {
                return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "No se puede eliminar el proceso porque tiene relaciones con otros registros.", "eliminarProceso " + ex.getMessage());
            }
            LOG.log(Level.SEVERE, "Ocurrio un error al eliminar el proceso.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al eliminar el proceso.", "eliminarProceso " + ex.getMessage());
        }
    }

}
