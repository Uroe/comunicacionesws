/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorcomunicacionesws.controller;

import cr.ac.una.gestorcomunicacionesws.model.CorreoDto;
import cr.ac.una.gestorcomunicacionesws.service.CorreoService;
import cr.ac.una.gestorcomunicacionesws.util.CodigoRespuesta;
import cr.ac.una.gestorcomunicacionesws.util.Correo;
import cr.ac.una.gestorcomunicacionesws.util.MachoteClave;
import cr.ac.una.gestorcomunicacionesws.util.MachoteNoti;
import cr.ac.una.gestorcomunicacionesws.util.Respuesta;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author Usuario
 */
@Path("/CorreoController")
public class CorreoController {

    @EJB
    private CorreoService correoService;

    @Context
    SecurityContext securityContext;

    @GET
    @Path("/correo/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCorreo(@PathParam("id") Long id) {
        try {
            Respuesta res = correoService.getCorreo(id);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok((CorreoDto) res.getResultado("Correo")).build();
        } catch (Exception ex) {
            Logger.getLogger(CorreoController.class
                    .getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo el correo").build();//TODO
        }
    }

    @GET
    @Path("/correos")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getCorreos() {
        try {
            Respuesta res = correoService.getCorreos();
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok(new GenericEntity<List<CorreoDto>>((List<CorreoDto>) res.getResultado("Correos")) {
            }).build();
        } catch (Exception ex) {
            Logger.getLogger(CorreoController.class
                    .getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo los correos").build();
        }
    }

    @POST
    @Path("/correo")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response guardarCorreo(CorreoDto correoDto) {
        try {
            Respuesta respuesta = correoService.guardarCorreo(correoDto);
            if (!respuesta.getEstado()) {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok((CorreoDto) respuesta.getResultado("Correo")).build();
        } catch (Exception ex) {
            Logger.getLogger(CorreoController.class
                    .getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error guardando el correo").build();
        }
    }

    @DELETE
    @Path("/correo/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response eliminarCorreo(@PathParam("id") Long id) {
        try {
            Respuesta respuesta = correoService.eliminarCorreo(id);
            if (!respuesta.getEstado()) {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok().build();
        } catch (Exception ex) {
            Logger.getLogger(CorreoController.class
                    .getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error eliminando el correo").build();
        }
    }

    @GET
    @Path("/correoclave/{para}/{claveTemp}/{usuario}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response enviarCorreoClave(@PathParam("para") String para, @PathParam("claveTemp") String clave, @PathParam("usuario") String usuario) {
        try {
            Correo correo = new Correo();
            MachoteClave machote = new MachoteClave(usuario, clave);
            correo.enviarCorreo(para, "Cambio de contraseña", machote.getMachote1());
            return Response.ok().build();
        } catch (Exception ex) {
            Logger.getLogger(CorreoController.class
                    .getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error enviando el correo").build();
        }
    }

    @GET
    @Path("/correo/{para}/{mensaje}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response notificarGestiones(@PathParam("para") String para, @PathParam("mensaje") String mensaje) {
        try {
            Correo correo = new Correo();
            MachoteNoti machote = new MachoteNoti(mensaje);
            correo.enviarCorreo(para, "Estado de la gestion", machote.getMachote2());
            return Response.ok().build();
        } catch (Exception ex) {
            Logger.getLogger(CorreoController.class
                    .getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error enviando el correo").build();
        }
    }
}
