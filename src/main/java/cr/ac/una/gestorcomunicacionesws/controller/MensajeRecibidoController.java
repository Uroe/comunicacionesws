/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorcomunicacionesws.controller;

import cr.ac.una.gestorcomunicacionesws.model.MensajeRecibidoDto;
import cr.ac.una.gestorcomunicacionesws.service.MensajeRecibidoService;
import cr.ac.una.gestorcomunicacionesws.util.CodigoRespuesta;
import cr.ac.una.gestorcomunicacionesws.util.Respuesta;
import cr.ac.una.gestorcomunicacionesws.util.Secure;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Usuario
 */
@Path("/MensajeRecibidoController")
public class MensajeRecibidoController {

    @EJB
    MensajeRecibidoService msjService;

    @GET
    @Path("/mensajerecibido/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMensajeRecibido(@PathParam("id") Long id) {
        try {
            Respuesta res = msjService.getMensajeRecibido(id);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok((MensajeRecibidoDto) res.getResultado("MensajeRecibido")).build();
        } catch (Exception ex) {
            Logger.getLogger(MensajeRecibidoController.class
                    .getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo el mensaje").build();
        }
    }

    @GET
    @Path("/mensajesrecibidos/{idReceptor}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getMensajesRecibidos(@PathParam("idReceptor") Long idReceptor) {
        try {
            Respuesta res = msjService.getMensajesRecibidos(idReceptor);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok(new GenericEntity<List<MensajeRecibidoDto>>((List<MensajeRecibidoDto>) res.getResultado("MensajesRecibidos")) {
            }).build();
        } catch (Exception ex) {
            Logger.getLogger(MensajeRecibidoController.class
                    .getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo los mensajes").build();
        }
    }

    @POST
    @Path("/mensajerecibido")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response guardarMensajeRecibido(MensajeRecibidoDto mensajeRecibidoDto) {
        try {
            Respuesta respuesta = msjService.guardarMensajeRecibido(mensajeRecibidoDto);
            if (!respuesta.getEstado()) {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok((MensajeRecibidoDto) respuesta.getResultado("MensajeRecibido")).build();
        } catch (Exception ex) {
            Logger.getLogger(MensajeRecibidoController.class
                    .getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error guardando el mensaje").build();
        }
    }

    @DELETE
    @Path("/mensajerecibido/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response eliminarMensajeRecibido(@PathParam("id") Long id) {
        try {
            Respuesta respuesta = msjService.eliminarMensajeRecibido(id);
            if (!respuesta.getEstado()) {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok().build();
        } catch (Exception ex) {
            Logger.getLogger(MensajeRecibidoController.class
                    .getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error eliminando el mensaje").build();
        }
    }

}
