/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorcomunicacionesws.controller;

import cr.ac.una.gestorcomunicacionesws.model.MensajeEnviadoDto;
import cr.ac.una.gestorcomunicacionesws.service.MensajeEnviadoService;
import cr.ac.una.gestorcomunicacionesws.util.CodigoRespuesta;
import cr.ac.una.gestorcomunicacionesws.util.Respuesta;
import cr.ac.una.gestorcomunicacionesws.util.Secure;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Usuario
 */
@Path("/MensajeEnviadoController")
public class MensajeEnviadoController {

    @EJB
    MensajeEnviadoService msjService;

    @GET
    @Path("/mensajeenviado/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMensajeEnviado(@PathParam("id") Long id) {
        try {
            Respuesta res = msjService.getMensajeEnviado(id);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok((MensajeEnviadoDto) res.getResultado("MensajeEnviado")).build();
        } catch (Exception ex) {
            Logger.getLogger(MensajeEnviadoController.class
                    .getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo el mensaje").build();
        }
    }

    @GET
    @Path("/mensajesenviados/{idEmisor}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getMensajesEnviados(@PathParam("idEmisor") Long idEmisor) {
        try {
            Respuesta res = msjService.getMensajesEnviados(idEmisor);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok(new GenericEntity<List<MensajeEnviadoDto>>((List<MensajeEnviadoDto>) res.getResultado("MensajesEnviados")) {
            }).build();
        } catch (Exception ex) {
            Logger.getLogger(MensajeEnviadoController.class
                    .getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo los mensajes").build();
        }
    }

    @POST
    @Path("/mensajeenviado")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response guardarMensajeEnviado(MensajeEnviadoDto mensajeEnviadoDto) {
        try {
            Respuesta respuesta = msjService.guardarMensajeEnviado(mensajeEnviadoDto);
            if (!respuesta.getEstado()) {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok((MensajeEnviadoDto) respuesta.getResultado("MensajeEnviado")).build();
        } catch (Exception ex) {
            Logger.getLogger(MensajeEnviadoController.class
                    .getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error guardando el mensaje").build();
        }
    }

    @DELETE
    @Path("/mensajeenviado/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response eliminarMensajeEnviado(@PathParam("id") Long id) {
        try {
            Respuesta respuesta = msjService.eliminarMensajeEnviado(id);
            if (!respuesta.getEstado()) {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok().build();
        } catch (Exception ex) {
            Logger.getLogger(MensajeEnviadoController.class
                    .getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error eliminando el mensaje").build();
        }
    }
}
