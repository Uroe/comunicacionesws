/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorcomunicacionesws.controller;

import cr.ac.una.gestorcomunicacionesws.model.ProcesoDto;
import cr.ac.una.gestorcomunicacionesws.service.ProcesoService;
import cr.ac.una.gestorcomunicacionesws.util.CodigoRespuesta;
import cr.ac.una.gestorcomunicacionesws.util.Respuesta;
import cr.ac.una.gestorcomunicacionesws.util.Secure;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Usuario
 */
@Path("/ProcesoController")
public class ProcesoController {

    @EJB
    private ProcesoService procesoService;

    @GET
    public Response ping() {
        return Response
                .ok("ping")
                .build();
    }

    @GET
    @Path("/proceso/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProceso(@PathParam("id") Long id) {
        try {
            Respuesta res = procesoService.getProceso(id);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok((ProcesoDto) res.getResultado("Proceso")).build();
        } catch (Exception ex) {
            Logger.getLogger(ProcesoController.class
                    .getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo el proceso").build();//TODO
        }
    }

    @GET
    @Path("/procesos")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getProcesos() {
        try {
            Respuesta res = procesoService.getProcesos();
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok(new GenericEntity<List<ProcesoDto>>((List<ProcesoDto>) res.getResultado("Procesos")) {
            }).build();
        } catch (Exception ex) {
            Logger.getLogger(ProcesoController.class
                    .getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo los procesos").build();
        }
    }

    @POST
    @Path("/proceso")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response guardarProceso(ProcesoDto proceso) {
        try {
            Respuesta respuesta = procesoService.guardarProceso(proceso);
            if (!respuesta.getEstado()) {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok((ProcesoDto) respuesta.getResultado("Proceso")).build();
        } catch (Exception ex) {
            Logger.getLogger(ProcesoController.class
                    .getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error guardando el proceso").build();
        }
    }

    @DELETE
    @Path("/proceso/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response eliminarProceso(@PathParam("id") Long id) {
        try {
            Respuesta respuesta = procesoService.eliminarProceso(id);
            if (!respuesta.getEstado()) {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok().build();
        } catch (Exception ex) {
            Logger.getLogger(ProcesoController.class
                    .getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error eliminando el proceso").build();
        }
    }
}
