/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorcomunicacionesws.controller;

import cr.ac.una.gestorcomunicacionesws.model.ConversacionDto;
import cr.ac.una.gestorcomunicacionesws.service.ConversacionService;
import cr.ac.una.gestorcomunicacionesws.util.CodigoRespuesta;
import cr.ac.una.gestorcomunicacionesws.util.Respuesta;
import cr.ac.una.gestorcomunicacionesws.util.Secure;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Usuario
 */
@Path("/ConversacionController")
public class ConversacionController {

    @EJB
    ConversacionService conversacionService;

    @GET
    public Response ping() {
        return Response
                .ok("ping")
                .build();
    }

    @GET
    @Path("/conversacion/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getConversacion(@PathParam("id") Long id) {
        try {
            Respuesta res = conversacionService.getConversacion(id);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok((ConversacionDto) res.getResultado("Conversacion")).build();
        } catch (Exception ex) {
            Logger.getLogger(ConversacionController.class
                    .getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo la conversacion").build();
        }
    }

    @GET
    @Path("/conversaciones/{idUsuario}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getConversaciones(@PathParam("idUsuario") Long idUsuario) {
        try {
            Respuesta res = conversacionService.getConversaciones(idUsuario);
            if (!res.getEstado()) {
                return Response.status(res.getCodigoRespuesta().getValue()).entity(res.getMensaje()).build();
            }
            return Response.ok(new GenericEntity<List<ConversacionDto>>((List<ConversacionDto>) res.getResultado("Conversaciones")) {
            }).build();
        } catch (Exception ex) {
            Logger.getLogger(ConversacionController.class
                    .getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error obteniendo las conversaciones").build();
        }
    }

    @POST
    @Path("/conversacion")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response guardarConversacion(ConversacionDto conversacionDto) {
        try {
            Respuesta respuesta = conversacionService.guardarConversacion(conversacionDto);
            if (!respuesta.getEstado()) {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok((ConversacionDto) respuesta.getResultado("Conversacion")).build();
        } catch (Exception ex) {
            Logger.getLogger(ConversacionController.class
                    .getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error guardando la conversacion").build();
        }
    }

    @DELETE
    @Path("/conversacion/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response eliminarConversacion(@PathParam("id") Long id) {
        try {
            Respuesta respuesta = conversacionService.eliminarConversacion(id);
            if (!respuesta.getEstado()) {
                return Response.status(respuesta.getCodigoRespuesta().getValue()).entity(respuesta.getMensaje()).build();
            }
            return Response.ok().build();
        } catch (Exception ex) {
            Logger.getLogger(ConversacionController.class
                    .getName()).log(Level.SEVERE, null, ex);
            return Response.status(CodigoRespuesta.ERROR_INTERNO.getValue()).entity("Error eliminando la conversacion").build();
        }
    }
}
